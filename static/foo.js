function get_thingy() {
    var promise = Kinvey.DataStore.get('test', 'foo_id',
    {
        success: function(response) {
            console.log("got it!");
            console.log(response);
        },
        error: function(response) {
            console.log("get error");
            console.log(response);
        }
    });

}

function do_stuff() {
    var promise = Kinvey.DataStore.save('test', {
        _id  : 'foo_id',
        fart : 'barf'
    }, {
        success: function(response) {
            alert("stored");
            get_thingy();
        },
        error: function(response) {
            console.log("save error");
            console.log(response);
        }
    });
}

function create_user() {
    var promise = Kinvey.User.signup({
        username : 'user1',
        password : 'password'
    }, {
        success: function(response) {
            alert("success");
        },
        error: function(response) {
            alert("create user error");
            console.log(response);
        }
    });
}

function login_user() {
    console.log("gonna log in");
    var promise = Kinvey.User.login({
        username : 'user1',
        password : 'password'
    }, {
        success: function(response) {
            // alert("success");
        },
        error: function(response) {
            alert("login user error");
            console.log(response);
        }
    });
}

function handle_kinvey_initialized() {
    // create_user();
    var user = Kinvey.getActiveUser();
    if( user == null ) {
        login_user();
    }

    var user = Kinvey.getActiveUser();
    console.log(user);

    do_stuff();


}

function init_kinvey(callback) {
    var promise = Kinvey.init({
        appKey    : 'kid_VexZ2on4H9',
        appSecret : '97375fa477b04dfa9e46bc5b157ab177'
    });
    promise.then(function(activeUser) {
        //handle_kinvey_initialized();
        if( callback != null ) {
            callback();
        }

    }, function(error) {
        alert("failure...");
    }); 
}



