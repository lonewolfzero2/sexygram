/**
 * @jsx React.DOM
 */
var CommentBox = React.createClass({
  render: function() {
    return (
      <div className="commentBox">
        Hello, world! I am a CommentBox2.
      </div>
    );
  }
});

var Photo = React.createClass({
    render: function() {
        return (
            <div className="photo">
                <br/>
                {this.props.author} | age of post
                <br/>
                PHOTO
                <br/>
                PHOTO
                <br/>
                PHOTO
                <br/>
                {this.props.children}
            </div>
            );
    }

});

var PhotoList = React.createClass({
    loadPhotoListFromServer: function() {
        /*
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            success: function(data) {
                this.setState({data: data});
            }.bind(this)
        });
        */
    },
    getInitialState: function() {
        return {photos: [ {author:"author1", txt:"Text1"}, {author:"author2", txt:"Text2"}, {author:"author3", txt:"Text3"} ]};
    },
    componentWillMount: function() {
        this.loadPhotoListFromServer();
        // setInterval(this.loadPhotoListFromServer, this.props.pollInterval);
    },
    render: function() {
        var photoNodes = this.state.photos.map( function(photo) {
            return <Photo author={photo.author}>{photo.txt}</Photo>
        });
        return (
            <div className="photoList">
                {photoNodes}
            </div>
            );
    }
});
