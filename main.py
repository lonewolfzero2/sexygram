from jinja2.exceptions import TemplateNotFound
import time
import datetime
import flask

app = flask.Flask(__name__, static_folder='static', static_url_path='/static')

@app.errorhandler(404)
def page_not_found(error):
    return '<h2>Error 404</h2><br/><br/>This page does not exist.<br/><br/><a href="/">Click here to return to the home page</a>.', 404

@app.route('/')
def page_home(): 
    context = {}
    return flask.render_template('home.html', **context )

@app.route('/explore')
def page_explore(): 
    context = {}
    return flask.render_template('explore.html', **context )

@app.route('/camera')
def page_camera(): 
    context = {}
    return flask.render_template('camera.html', **context )

@app.route('/news')
def page_news(): 
    context = {}
    return flask.render_template('news.html', **context )

@app.route('/profile')
def page_profile(): 
    context = {}
    return flask.render_template('profile.html', **context )

if __name__ == '__main__':
    print ("*** Starting")
    app.run(host='0.0.0.0', debug=True, use_reloader=True)
